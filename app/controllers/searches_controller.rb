class SearchesController < ApplicationController
	def index
	end
	
	def results
		searchTerm = params[:searchTerm]
		response = HTTParty.get("https://www.googleapis.com/customsearch/v1?q=#{searchTerm}&cx=004748541299913983241%3Abjwhjlpsufk&key=AIzaSyBOaPFCrFeKj9ys7__AbfRkGpbDE-_-4Xw")
		@result = JSON.parse(response.body)
		render plain: @result, status: 200
	end
end
