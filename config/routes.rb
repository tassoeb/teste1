Rails.application.routes.draw do
 get 'results', to: 'searches#results'
 root 'searches#index' 
end
